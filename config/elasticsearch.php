<?php

return [

    'hosts' => explode(',', env('ELASTICSEARCH_HOSTS')),

    'index' => [
        'body' => [
            'settings' => [
                'analysis' => [
                    'filter' => [
                        'persian_stemmer' => [
                            'pattern' => '(?<=\S{2,})(ی{1,2}|(ها)|(تر)|(ترین)|(ان)|(ات))(?=(\s|$))',
                            'type' => 'pattern_replace',
                            'replacement' => ''
                        ],
                        'persian_stem_multiplexer' => [
                            'type' => 'multiplexer',
                            'preserve_original' => 'true',
                            'filters' => [
                                'persian_stemmer'
                            ]
                        ],
                        'persian_stop_word' => [
                            'type' => 'stop',
                            'stopwords_path' => 'stopwords.txt'
                        ]
                    ],
                    'char_filter' => [
                        'zero_width_non_joiner_to_space' => [
                            'type' => 'mapping',
                            'mappings' => ['\u200C=>\u0020']
                        ],
                        'persian_normalize_mapper' => [
                            'type' => 'mapping',
                            'mappings' => [
                                'آ => ا',
                                'أ => ا',
                                'إ => ا',
                                'ؤ => و',
                                'ي => ی',
                                'ئ => ی',
                                'ك => ک',
                                'ة => ه',
                                'ۂ => ه'
                            ]
                        ],
                        'persian_normalize_replacer' => [
                            'pattern' => '(ـ|ء|ً|ٌ|ٍ|َ|ُ|ِ|ّ|ْ)',
                            'type' => 'pattern_replace',
                            'replacement' => ''
                        ]
                    ],
                    'analyzer' => [
                        'title_search_analyzer' => [
                            'filter' => ['lowercase', 'persian_stem_multiplexer', 'persian_stop_word'],
                            'char_filter' => [
                                'persian_normalize_replacer',
                                'persian_normalize_mapper',
                                'zero_width_non_joiner_to_space'
                            ],
                            'tokenizer' => 'standard'
                        ]
                    ],
                ]
            ],
            'mappings' => [
                'properties' => [
                    'title' => [
                        'type' => 'text',
                        'fielddata' => 'true',
                        'fields' => [
                            'raw' => [
                                'type' => 'keyword',
                            ]
                        ],
                        'analyzer' => 'title_search_analyzer',
                        'search_analyzer' => 'title_search_analyzer'
                    ]
                ]
            ]
        ]
    ],
];
