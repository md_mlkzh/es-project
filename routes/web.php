<?php

use App\Http\Controllers\ESController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::name('index.')
    ->controller(ESController::class)
    ->group(function () {

        Route::get('/', 'list')->name('list');
        Route::get('/create-index', 'create')->name('create');
        Route::post('/store-indexes', 'store')->name('store');
        Route::get('/search-index', 'search')->name('search');
    });
