<?php

namespace App\Http\Controllers;

use App\Adapters\Contracts\SearchEngineContract;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ESController extends Controller
{
    private SearchEngineContract $searchEngineClient;

    public function __construct(SearchEngineContract $searchEngineContract)
    {
        $this->searchEngineClient = $searchEngineContract;
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function create(): \Illuminate\Foundation\Application|View|Factory|Application
    {
        return view('indices.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        try {

            $this->searchEngineClient->createIndex($request->input('index_name'));

            $this->searchEngineClient->createIndexData($request->input('index_name'), $request->input('json_data'));

            $status = [
                'successful' => 'The index created.'
            ];

        } catch (\Exception $e) {

            info($e->getMessage());

            $status = [
                'failed' => 'There is a problem. Check the log file.'
            ];
        }

        return redirect()
            ->route('index.create')
            ->with($status);
    }

    /**
     * @return View|\Illuminate\Foundation\Application|Factory|Application
     */
    public function list(): View|\Illuminate\Foundation\Application|Factory|Application
    {
        return view('indices.list', [
            'indices' => $this->searchEngineClient->list(),
        ]);
    }

    /**
     * @param Request $request
     * @return View|\Illuminate\Foundation\Application|Factory|Application
     */
    public function search(Request $request): View|\Illuminate\Foundation\Application|Factory|Application
    {
        return view('indices.search', [
            'indexName' => $request->input('index_name'),
        ]);
    }
}
