<?php

namespace App\Http\Controllers;

use App\Adapters\Contracts\SearchEngineContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchController
{
    /**
     * searches the value in search engine
     *
     * @param SearchEngineContract $searchEngineContract
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(SearchEngineContract $searchEngineContract, Request $request)
    {
        $searchHits = $searchEngineContract->search(
            $request->query('q'),
            $request->query('size'),
            $request->query('index_name')
        );

        return response()->json($searchHits);
    }


}
