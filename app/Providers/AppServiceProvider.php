<?php

namespace App\Providers;

use App\Adapters\Contracts\SearchEngineContract;
use App\Adapters\ElasticsearchContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        app()->bind(
            SearchEngineContract::class,
            ElasticsearchContract::class
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
