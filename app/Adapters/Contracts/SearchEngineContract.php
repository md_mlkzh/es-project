<?php


namespace App\Adapters\Contracts;


interface SearchEngineContract
{
    public function search(string $value, int $size, string $indexName): array;
}
