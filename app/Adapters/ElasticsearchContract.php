<?php

namespace App\Adapters;

use App\Adapters\Contracts\SearchEngineContract;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\MissingParameterException;
use Elastic\Elasticsearch\Exception\ServerResponseException;

class ElasticsearchContract implements SearchEngineContract
{
    /**
     * @var Client
     */
    private Client $elasticsearchClient;

    /**
     * ElasticsearchContract constructor.
     * @throws AuthenticationException
     */
    public function __construct()
    {
        $this->elasticsearchClient = ClientBuilder::create()
            ->setSSLVerification(false)
            ->setHosts(config('elasticsearch.hosts'))
            ->setBasicAuthentication('elastic', 'hyYwggU3TnPHoiRgIT+v')
            ->setCABundle('http_ca.crt')
            ->build();
    }

    /**
     * searches the given value in elasticsearch indices
     *
     * @param string $value
     * @param int $size
     * @param string $indexName
     * @return array
     * @throws ClientResponseException
     * @throws ServerResponseException
     */
    public function search(string $value, int $size, string $indexName): array
    {
        $hits = $this->elasticsearchClient->search([
            'index' => $indexName,
            'size' => $size,
            '_source' => ['title'],
            'body' => [
                'query' => [
                    'multi_match' => [
                        'fields' => [
                            'title'
                        ],
                        'query' => $value,
                    ]
                ],
                'aggs' => [
                    'title_count' => [
                        'terms' => [
                            'field' => 'title',
                        ]
                    ]
                ]
            ]
        ]);

        return [
            'results' => collect($hits['hits']['hits'])
                ->transform(function ($hit) use ($hits) {
                    return array_merge(
                        $hit['_source'],
                        [
                            '_score' => $hit['_score']
                        ]
                    );
                })
                ->toArray(),
            'wordcloud' => $hits['aggregations']['title_count']['buckets'],
        ];
    }

    /**
     * @param string $indexName
     * @return true
     * @throws ClientResponseException
     * @throws MissingParameterException
     * @throws ServerResponseException
     */
    public function createIndex(string $indexName): bool
    {
        $this->elasticsearchClient->indices()->create([
            'index' => $indexName,
            'body' => config('elasticsearch.index.body')
        ]);

        return true;
    }

    /**
     * @param string $indexName
     * @param string $data
     * @return true
     * @throws ClientResponseException
     * @throws ServerResponseException
     */
    public function createIndexData(string $indexName, string $data): bool
    {

        $arrayData = json_decode($data, true);

        $params = ['body' => []];

        foreach ($arrayData as $indexData) {

            $params['body'][] = [
                'index' => [
                    '_index' => $indexName
                ]
            ];

            $params['body'][] = [
                'title' => $indexData['title'],
            ];
        }

        $this->elasticsearchClient->bulk($params);

        return true;
    }

    public function list()
    {
        $rawIndexes = $this->elasticsearchClient->cat()->indices([
            's' => 'index',
            'format' => 'json',
        ])->asArray();

        return array_filter($rawIndexes, function ($rawIndex) {
            return !preg_match('/^\./', $rawIndex['index']);
        });
    }
}
