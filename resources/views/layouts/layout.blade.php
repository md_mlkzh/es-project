<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>elasticsearch</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://kit.fontawesome.com/54c0875e1b.js" crossorigin="anonymous"></script>
    <script src="{{ asset("js/JQuery.js") }}"></script>

    @include('navbar')
</head>
<body>

<div class="container">

    @if(\Illuminate\Support\Facades\Session::has('successful'))
        <div class="alert alert-success alert-trim" role="alert">
            {{ \Illuminate\Support\Facades\Session::get('successful') }}
        </div>
    @elseif(\Illuminate\Support\Facades\Session::has('failed'))
        <div class="alert alert-danger alert-trim" role="alert">
            {{ \Illuminate\Support\Facades\Session::get('failed') }}
        </div>
    @endif

    @yield('content')
</div>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
