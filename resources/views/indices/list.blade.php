@extends('layouts/layout')

@section('content')
    <table class="table table-light table-bordered table-hover mt-5">
        <thead>
        <tr class="text-center">
            <th scope="col">index name</th>
            <th scope="col">index health</th>
            <th scope="col">index status</th>
            <th scope="col">docs count</th>
            <th scope="col">actions</th>
        </tr>
        </thead>
        <tbody>

        @if(!empty($indices))
            @foreach($indices as $key => $index)
                <tr class="text-center">
                    <td>{{ $index['index'] }}</td>
                    <td>{{ $index['health'] }}</td>
                    <td>{{ $index['status'] }}</td>
                    <td>{{ $index['docs.count'] }}</td>
                    <td>
                        <div class="actions">
                            <a href="{{ route('index.search', ['index_name' => $index['index']]) }}">
                                <i class="fas fa-search" style="color: #2992c7;"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

@endsection
