@extends('layouts.layout')

@section('content')

    @if($indexName)
        <h4 style="text-align: center; margin: 15px 0">
            You are searching on <span class="badge bg-info">{{ $indexName }}</span> index.
        </h4>
    @endif
    <div class="search-cloud" style="direction: rtl">
        <div class="search-part">
            <div id="search-bar" class="input-group flex-nowrap">
                <input type="text" class="form-control" placeholder="جستجو" aria-label="search"
                       aria-describedby="addon-wrapping">
            </div>
            <section id="search-results" class="">
                <div class="search-result-item sample">
                    <div class="main-text">
                    </div>
                    <div class="score">
                    </div>
                </div>

            </section>
        </div>
        <div class="cloud-part">
            <ul class="cloud" role="navigation" id="cloud" aria-label="Webdev word cloud">
                <li><a href="#" data-weight="4"></a></li>
            </ul>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on("keyup", "#search-bar input", function () {

                let query = $("#search-bar input").val();
                let index_name = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')[0].split('=')[1];

                if (query.length > 2) {
                    $.ajax({
                        method: "GET",
                        url: "/api/search?q=" + query + "&size=10" + "&index_name=" + index_name,
                        dataType: "json"
                    }).done(function (response) {

                        $("#search-results").removeClass("open");
                        $("#search-results .search-result-item:not(.sample)").remove();
                        $("#cloud").empty();

                        $.each(response.results, function (key, record) {
                            $("#search-results").append(
                                $("#search-results .search-result-item.sample:first")
                                    .clone().removeClass("sample")
                                    .find(".main-text").html(record.title).end()
                                    .find(".score").html("score: " + record._score).end()
                            )
                        });

                        $.each(response.wordcloud, function (key, word) {

                            let frequency = word.doc_count > 9 ? 9 : word.doc_count;

                            $("#cloud").append(
                                $("<li>").append(
                                    $("<a>").attr("href", "#").attr("data-weight", frequency)
                                        .text(word.key + "(" + word.doc_count + ")")
                                        .css("text-align", "center")
                                )
                            )
                        })

                        $("#search-results").addClass("open");
                    });
                } else {
                    $("#search-results").removeClass("open");
                    $("#search-results .search-result-item:not(.sample)").remove();
                    $("#cloud").empty();
                }
            })
        });
    </script>

@endsection
