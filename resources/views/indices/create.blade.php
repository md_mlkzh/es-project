@extends('layouts/layout')

@section('content')
    <form method="post" action="{{ route('index.store') }}" class="mt-4 mx-auto w-50">
        @csrf
        <div class="form-group mb-3">
            <label for="indexName" class="form-label">Index name</label>
            <input type="text" class="form-control" id="indexName" name="index_name" placeholder="index name">
        </div>
        <div class="mb-3">
            <label for="jsonData" class="form-label">Insert index data</label>
            <textarea class="form-control" id="jsonData" name="json_data" rows="15"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Store</button>
    </form>
@endsection
