<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="{{ route('index.list') }}">Index List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('index.create') }}">Create Index</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
